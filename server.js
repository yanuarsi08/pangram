'use strict';

const Hapi = require('@hapi/hapi');
const Plugins = require('./plugins');
const Route = require('./routes');

const server = Hapi.server({
    port: process.env.PORT || 3000,
    host: '0.0.0.0'
});

exports.init = async () => {

    await Route.register(server);
    await server.initialize();
    return server;
};

exports.start = async () => {

    await Plugins.register(server);
    await Route.register(server);
    await server.start();
    return server;
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});
