'use strict';

const Lab = require('@hapi/lab');

const { expect } = require('@hapi/code');

const { afterEach, beforeEach, describe, it } = exports.lab = Lab.script();

const { init } = require('../server');

let server;

beforeEach(async () => {

    server = await init();
});

afterEach(async () => {

    await server.stop();
});

describe('GET /', () => {

    it('call get responds with 200', async () => {

        const res = await server.inject({
            method: 'get',
            url: '/'
        });
        expect(res.statusCode).to.equal(200);

        const pangramResponse = await server.inject({
            method: 'get',
            url: '/pangram'
        });

        expect(pangramResponse.statusCode).to.equal(200);

        const isogramResponse = await server.inject({
            method: 'get',
            url: '/isogram'
        });

        expect(isogramResponse.statusCode).to.equal(200);

        const injectAnagramOptions = {
            method: 'get',
            url: '/validAnagram?firstLetter=listen&secondaryLetter=silent'
        };

        const validAnagramResponse = await server.inject(injectAnagramOptions);

        expect(validAnagramResponse.statusCode).to.equal(200);

        const anagramResponse = await server.inject({
            method: 'get',
            url: '/anagram'
        });

        expect(anagramResponse.statusCode).to.equal(200);

        const injectIsogramOptions = {
            method: 'get',
            url: '/validIsogram?letter=listen'
        };

        const validIsogramResponse = await server.inject(injectIsogramOptions);

        expect(validIsogramResponse.statusCode).to.equal(200);
    });
});
