'use strict';

const Pangram = require('../repository/pangram_handler');
const Joi = require('@hapi/joi');

module.exports.register = async (server) => {

    await server.route([{
        method: 'GET',
        path: '/pangram',
        options: {
            handler: async function () {

                return await Pangram.getRandomPangram().then((response) => response);
            },
            description: 'Get Random Pangram',
            notes: 'Returns Random Pangram',
            tags: ['api']
        }
    }, {
        method: 'GET',
        path: '/',
        handler: function () {

            return 'Hello World!';
        }
    }, {
        method: 'GET',
        path: '/validAnagram',
        options: {
            handler: async function (request, h) {

                const params = request.query;
                return await Pangram.isValidAnagram(params.firstLetter, params.secondaryLetter).then((response) => response);
            },
            description: 'Get Valid Anagram',
            notes: 'Get Valid',
            tags: ['api'],
            response: {
                status: {
                    200: Joi.object({
                        firstLetter: Joi.string(),
                        secondaryLetter: Joi.string(),
                        isAnagram: Joi.boolean(),
                        reason: Joi.string()
                    }).label('ValidAnagram'),
                    400: Joi.any()
                }
            },
            validate: {
                query: Joi.object({
                    firstLetter: Joi.string(),
                    secondaryLetter: Joi.string()
                })
            }
        }
    }, {
        method: 'GET',
        path: '/anagram',
        options: {
            handler: async function () {

                return await Pangram.getAnagram().then((response) => response);
            },
            description: 'Get Random Anagram',
            notes: 'Returns Random Anagram',
            tags: ['api'],
            response: {
                status: {
                    200: Joi.object({
                        _id: Joi.string(),
                        count: Joi.number(),
                        words: Joi.array().items(Joi.string())
                    }).label('RandomAnagram'),
                    400: Joi.any()
                }
            }
        }
    },
    {
        method: 'GET',
        path: '/validIsogram',
        options: {
            handler: async function (request, h) {

                const params = request.query;
                return await Pangram.isValidIsogram(params.letter).then((response) => response);
            },
            description: 'Get Valid Isogram',
            notes: 'Get Valid Isogram',
            tags: ['api'],
            response: {
                status: {
                    200: Joi.object({
                        letter: Joi.string(),
                        isIsogram: Joi.boolean(),
                        reason: Joi.string()
                    }).label('ValidIsogram'),
                    400: Joi.any()
                }
            },
            validate: {
                query: Joi.object({
                    letter: Joi.string()
                })
            }
        }
    },
    {
        method: 'GET',
        path: '/isogram',
        options: {
            handler: async function () {

                return await Pangram.getRandomIsogram().then((response) => response);
            },
            description: 'Get Random Isogram',
            notes: 'Returns Random Isogram',
            tags: ['api'],
            response: {
                status: {
                    200: Joi.object({
                        word: Joi.string(),
                        isIsogram: Joi.boolean()
                    }).label('RandomIsogram'),
                    400: Joi.any()
                }
            }
        }
    }
    ]
    );
};
