'use strict';

const Blipp = require('blipp');
const Pino = require('hapi-pino');
const Inert = require('@hapi/inert');
const Vision = require('@hapi/vision');
const HapiSwagger = require('hapi-swagger');

const isDev = process.env.NODE_ENV !== 'production';

module.exports.register = async (server) => {

    await server.register([Blipp, {
        plugin: Pino,
        options: {
            prettyPrint: isDev,
            logEvents: ['response', 'onPostStart']
        }
    },Inert, Vision, {
        plugin: HapiSwagger,
        options: {
            info: {
                title: 'Pangram API Documentation',
                version: '0.1'
            }
        }
    }]);
};
