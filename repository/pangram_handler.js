'use strict';

const Mongo = require('mongodb');
const Unirest = require('unirest');
const Axios = require('axios');
const MClient = require('mongodb').MongoClient;

const url = 'mongodb+srv://yanuyanu:abcd@cluster0-06noh.mongodb.net/training?retryWrites=true&w=majority';
const INPUT = 'https://raw.githubusercontent.com/dwyl/english-words/master/words_dictionary.json';
const getRandomPangram = async function () {

    return await Mongo.connect(url).then(async (MongoClient) => {

        const result = await MongoClient.db('training').collection('pangram').aggregate(
            [{ $sample: { size: 1 } }]).toArray();
        return result;

    });
};

const isAnagram = function (firstLetter, secondaryLetter) {

    if (firstLetter.length === 0 || secondaryLetter === 0) {
        return false;
    }

    if (firstLetter.length !== secondaryLetter.length) {
        return false;
    }

    firstLetter = firstLetter.toLowerCase().split('').sort();
    secondaryLetter = secondaryLetter.toLowerCase().split('').sort();

    return JSON.stringify(firstLetter) === JSON.stringify(secondaryLetter);
};

const isValidWord = async function (letter) {

    return await Unirest.get('https://mashape-community-urban-dictionary.p.rapidapi.com/define').query({
        'term': letter
    }).headers({
        'x-rapidapi-host': 'mashape-community-urban-dictionary.p.rapidapi.com',
        'x-rapidapi-key': 'fc75808117msha5af0ca37376ca1p1e3c88jsne034b68ecf1a'
    });
};

const isValidAnagram = async function (firstLetter, secondaryLetter) {

    const reasonResult = [];
    const checkAnagram = isAnagram(firstLetter, secondaryLetter);
    let index = 0;
    if (!checkAnagram) {
        reasonResult[index++] = 'No valid Anagram';
        return toAnagramJson(firstLetter, secondaryLetter, false, reasonResult.join(', '));
    }

    const firstValidLetter = await isValidWord(firstLetter);

    if (firstValidLetter.body.list.length === 0) {

        reasonResult[index++] = 'First letter is not valid English';

    }

    const secondaryValidLetter = await isValidWord(secondaryLetter);

    if (secondaryValidLetter.body.list.length === 0) {

        reasonResult[index++] = 'Secondary letter is not valid English';

    }

    if (firstValidLetter.body.list.length > 0 && secondaryValidLetter.body.list.length > 0) {

        return toAnagramJson(firstLetter, secondaryLetter, true, '-');

    }

    return toAnagramJson(firstLetter, secondaryLetter, false, reasonResult.join(', '));

};

const toAnagramJson = function (first, second, result, reasonResult) {

    return {
        firstLetter: first,
        secondaryLetter: second,
        isAnagram: result,
        reason: reasonResult
    };
};

const toIsogramJson = function (first, result, reasonResult) {

    return {
        letter: first,
        isIsogram: result,
        reason: reasonResult
    };

};

const saveAnagramWords = function () {

    Axios.get(INPUT)
        .then((response) => {

            saveMany(generateAnagrams(Object.keys(response.data)), 'anagram');
        });
};

const saveIsogramWords = function () {

    Axios.get(INPUT)
        .then((response) => {

            saveMany(generateIsogram(Object.keys(response.data)), 'isogram');
        });
};

const generateAnagrams = function (responseData) {

    const initialParam = [];
    responseData.forEach((element) => {

        initialParam.push(toRandomAnagramJson(element));
    });
    return initialParam;
};

const generateIsogram = function (responseData) {

    const initialParam = [];
    responseData.forEach((element) => {

        initialParam.push(toRandomIsogramJson(element));
    });
    return initialParam;
};

const toRandomAnagramJson = function (responseData) {

    return {
        word: responseData,
        sort: responseData.toLowerCase().split('').sort().join(''),
        length: responseData.length
    };
};

const toRandomIsogramJson = function (responseData) {

    return {
        word: responseData,
        isIsogram: isIsogram(responseData)
    };
};

const saveMany = async (json, collection) => {

    await MClient.connect(url, (err, db) => {

        if (err) {
            throw err;
        }

        const dbo = db.db('training');

        dbo.collection(collection).insertMany(json, (err, res) => {

            if (err) {
                throw err;
            }

            db.close();

        });
    });
};

const getAnagram = async function () {

    return await Mongo.connect(url).then(async (MongoClient) => {

        const result = await MongoClient.db('training').collection('anagram').aggregate(
            [
                {
                    $group: {
                        _id: '$sort',
                        count: {
                            $sum: 1
                        },
                        words: {
                            $addToSet: '$word'
                        }
                    }
                },
                {
                    $match: {
                        'count': {
                            $gt: 1
                        }
                    }
                },
                {
                    $sample: {
                        size: 1
                    }
                }
            ]).toArray();
        return result[0];

    });
};

const isIsogram = (str) => {

    const isogram = new Set(str.toLowerCase().split(''));
    return str.length === isogram.size;
};

const isValidIsogram = async function (letter) {

    const reasonResult = [];
    const checkIsogram = isIsogram(letter);
    let index = 0;
    if (!checkIsogram) {
        reasonResult[index++] = 'No valid Isogram';
        return toIsogramJson(letter, false, reasonResult.join(', '));
    }

    const validLetter = await isValidWord(letter);

    if (validLetter.body.list.length === 0) {

        reasonResult[index++] = 'letter is not valid English';
        return toIsogramJson(letter, false, reasonResult.join(', '));

    }

    return toIsogramJson(letter, checkIsogram, ' ');

};

const getRandomIsogram = async function () {

    return await Mongo.connect(url).then(async (MongoClient) => {

        const result = await MongoClient.db('training').collection('isogram').aggregate(
            [
                {
                    $sample: {
                        size: 1
                    }
                },
                {
                    $project: {
                        _id: 0
                    }
                }
            ]).toArray();
        return result[0];

    });
};

module.exports = { getRandomPangram, isValidAnagram, saveAnagramWords, getAnagram, isValidIsogram, saveIsogramWords, getRandomIsogram };
